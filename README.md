## Easy Job Scheduler

### Build instructions

```bash
# Clone repository and change to cloned folder
# Then

# Prepare Makefiles
cmake -B build .

# Build binary
make -C build

# Run binary
./bin/job_scheduler <COMMAND> [TASK_FILE_0, ...]

```

