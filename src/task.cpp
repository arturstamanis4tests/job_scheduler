#include "task.h"

#include <cstdlib>
#include <iterator>
#include <regex>

// At this point at least two parameters will be available
task::task(const std::vector<std::string>& taskParams)
    : taskName_{taskParams[0]}
    , taskCommand_{taskParams[1]}
{
	if (taskParams.size() > 2) {
		const std::string& dependencies = taskParams[2];
		std::regex         commaSpaceRE(",\\s+");
		std::copy(std::sregex_token_iterator(dependencies.begin(), dependencies.end(), commaSpaceRE, -1),
		          std::sregex_token_iterator(),
		          std::back_inserter(taskDependencies_));
	}
}

void task::execute() const
{
	std::system(taskCommand_.c_str());
}

const std::string& task::taskName() const
{
	return taskName_;
}

const std::vector<std::string>& task::taskDependencies() const
{
	return taskDependencies_;
}
