#ifndef TASK_H_DEFINED
#define TASK_H_DEFINED
#include <functional>
#include <string>
#include <vector>

class task {
	std::string              taskName_;
	std::string              taskCommand_;
	std::vector<std::string> taskDependencies_;

public:
	// Default ones
	task()            = default;
	task(const task&) = default;
	task& operator=(const task&) = default;
	task(task&&)                 = default;
	task& operator=(task&&) = default;

	// Custom ctor
	task(const std::vector<std::string>& taskParams);

	void execute() const;

	const std::string&              taskName() const;
	const std::vector<std::string>& taskDependencies() const;
};

// Hash function for task
// Using it because tasks for execution will be kept in unordered_set container.
// Set relieves us from necessity to check if task already exists for execution.
// As we do not care about order of tasks - unordered_set for efficiency.
namespace std {

template<>
struct hash<task> {
	using argument_type = task;
	using result_type   = std::size_t;

	size_t operator()(const task& t) const
	{
		return std::hash<std::string>()(t.taskName());
	}
};

}  // namespace std

#endif
