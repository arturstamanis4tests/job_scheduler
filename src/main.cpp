#include <fstream>
#include <iostream>
#include <optional>
#include <string>
#include <unordered_set>
#include <vector>

#include "task.h"

std::optional<const task*> findTask(const std::vector<task>& tasks, const std::string& taskName)
{
	if (const auto taskIt = std::find_if(tasks.begin(),
	                                     tasks.end(),
	                                     [&taskName](const auto& task) {
		                                     return (task.taskName() == taskName);
	                                     });
	    taskIt == tasks.end()) {
		return {};
	}
	else {
		return &*taskIt;
	}
}

void populateTasks(const std::vector<task>&         tasks,
                   const task*                      thisTask,
                   std::unordered_set<const task*>& tasksToRun)
{
	const auto [insertedTask, isNew] = tasksToRun.insert(thisTask);
	if (!isNew) {
		return;
	}
	if (thisTask->taskDependencies().empty()) {
		return;
	}

	for (const auto& taskName : thisTask->taskDependencies()) {
		if (const auto subTaskPtr = findTask(tasks, taskName); !subTaskPtr) {
			throw taskName;
		}
		else {
			populateTasks(tasks, *subTaskPtr, tasksToRun);
		}
	}
}

int main(int argc, char** argv)
{
	if (argc < 3) {
		std::cerr << "Usage ./job_scheduler <COMMAND_NAME> [TASK_FILE_0, ...]" << std::endl;
		return 1;
	}

	std::vector<task> tasks;
	for (size_t fileIdx = 2; fileIdx < argc; ++fileIdx) {
		std::ifstream ifs(argv[fileIdx]);
		if (!ifs) {
			std::cerr << "Task file: " << argv[fileIdx] << " does not exists" << std::endl;
			continue;
		}

		std::vector<std::string> taskParams;
		std::string              line;
		while (std::getline(ifs, line)) {
			if (!line.empty()) {
				taskParams.push_back(line);
			}
		}

		if (taskParams.empty()) {
			std::cerr << "No parameters supplied in: " << argv[fileIdx] << std::endl;
			continue;
		}

		if (taskParams.size() < 2) {
			std::cerr << "Not enough parameters supplied in " << argv[fileIdx] << std::endl;
			continue;
		}

		tasks.emplace_back(taskParams);

		taskParams.clear();
	}

	if (tasks.empty()) {
		std::cerr << "Cannot find at least on valid task, leaving" << std::endl;
		return 1;
	}

	std::unordered_set<const task*> tasksToRun;

	std::string command{argv[1]};
	if (const auto taskPtr = findTask(tasks, command); !taskPtr) {
		std::cerr << "Error! Cannot find task '" << command << "' to run" << std::endl;
		return 1;
	}
	else {
		try {
			// Try to dive into every dependecy of task
			// If dependency is not found - throw
			populateTasks(tasks, *taskPtr, tasksToRun);
		}
		catch (const std::string& failedTask) {
			std::cerr << "Error! Cannot find dependent task: '" << failedTask << "' to run"
			          << std::endl;
			return 1;
		}
	}

	for (const auto& pTask : tasksToRun) {
		pTask->execute();
	}

	return 0;
}
